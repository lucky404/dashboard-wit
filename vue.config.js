module.exports = {
  transpileDependencies: ['vuetify'],
  publicPath: process.env.NODE_ENV === 'production' ? '/' : '/',
  pwa: {
    workboxPluginMode: 'GenerateSW',
    name: 'Dashboard Admin',
    background_color: '#f0f0f0',
    themeColor: '#f0f0f0',
    msTileColor: '#ffffff',
    appleMobileWebAppStatusBarStyle: 'white',
    manifestOptions: {
      name: 'Dashboard Admin',
      short_name: 'Dashboard Admin',
      background_color: '#f0f0f0',
      start_url: '.',
      display: 'standalone',
      theme_color: '#f0f0f0',
    },
  },
};
