import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
Vue.use(VueToast, { position: 'top-right' });

import '@/assets/css/main.css';

import Convert from './plugins/convert';
import VeeValidate from './plugins/veeValidate';
import VueApexCharts from './plugins/VueApexCharts';

Vue.config.productionTip = false;

import axios from 'axios';

axios.defaults.timeout = 1000 * 30;
axios.interceptors.response.use(
  (response) => {
    return response;
  },
  function (err) {
    return new Promise(function () {
      if (
        err.response &&
        err.response.status === 401 &&
        err.response.data.error_code === 'token_expired' &&
        err.config &&
        !err.config.__isRetryRequest
      ) {
        store.dispatch('clearSessions');
        router.push('/login');
      }
      throw err;
    });
  }
);

new Vue({
  router,
  store,
  Convert,
  VeeValidate,
  VueApexCharts,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
