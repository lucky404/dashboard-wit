import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: {
          base: '#4765b6',
          lighten1: '#f4f7ff',
          lighten2: '#f0f0f0',
        },
        accent: '#ff9f78',
        error: '#ff6968',
      },
    },
  },
});
