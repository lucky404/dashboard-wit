import { get, post, patch, destroy } from '@/services/Axios';
import qs from 'qs';
import errorHandler from '@/mixins/errorHandler';

export default {
  mixins: [errorHandler],
  data() {
    return {
      role: {},
      roles: [],
      loadRoles: true,
      loadFormRole: false,
      loadRole: false,
    };
  },
  methods: {
    async getRoles() {
      this.loadRoles = true;
      try {
        const { data } = await get('/roles');
        this.roles = data;
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadRoles = false;
      }
    },
    async getRole(id) {
      this.loadRole = true;
      try {
        const { data } = await get(`/roles/${id}`);
        this.role = data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadRole = false;
      }
    },
    async createRole(request) {
      this.loadFormRole = true;
      const requestBody = {
        name: request.name,
        create: request.create,
        update: request.update,
        delete: request.delete,
        view: request.view,
        created_at: new Date(),
        updated_at: new Date(),
      };
      const payload = qs.stringify(requestBody);
      try {
        const { data } = await post('/roles', {
          data: payload,
        });
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormRole = false;
      }
    },
    async updateRole(request, id) {
      this.loadFormRole = true;
      const requestBody = {
        name: request.name,
        create: request.create,
        update: request.update,
        delete: request.delete,
        view: request.view,
        updated_at: new Date(),
      };
      const payload = qs.stringify(requestBody);
      try {
        const { data } = await patch(`/roles/${id}`, {
          data: payload,
        });
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormRole = false;
      }
    },
    async deleteRole(id) {
      this.loadFormRole = true;
      try {
        await destroy(`/roles/${id}`);
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormRole = false;
      }
    },
  },
};
