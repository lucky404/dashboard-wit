import { get, post, patch, destroy } from '@/services/Axios';
import qs from 'qs';
import errorHandler from '@/mixins/errorHandler';

export default {
  mixins: [errorHandler],
  data() {
    return {
      menu: {},
      menus: [],
      loadMenus: true,
      loadFormMenu: false,
      loadMenu: false,
    };
  },
  methods: {
    async getMenus() {
      this.loadMenus = true;
      try {
        const { data } = await get('/menus');
        this.menus = data;
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadMenus = false;
      }
    },
    async getMenu(id) {
      this.loadMenu = true;
      try {
        const { data } = await get(`/menus/${id}`);
        this.menu = data;
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadMenu = false;
      }
    },
    async createMenu(request) {
      this.loadFormMenu = true;
      const requestBody = {
        name: request.name,
        level: request.level,
        folder: request.folder,
        parent: request.parent,
        description: request.description,
        isActive: request.isActive,
        sorting: request.sorting,
        icon: request.icon,
        create: request.create,
        update: request.update,
        delete: request.delete,
        view: request.view,
        created_at: new Date(),
        updated_at: new Date(),
      };
      const payload = qs.stringify(requestBody);
      try {
        const { data } = await post('/menus', {
          data: payload,
        });
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormMenu = false;
      }
    },
    async updateMenu(request, id) {
      this.loadFormMenu = true;
      const requestBody = {
        name: request.name,
        level: request.level,
        folder: request.folder,
        parent: request.parent,
        description: request.description,
        isActive: request.isActive,
        sorting: request.sorting,
        icon: request.icon,
        create: request.create,
        update: request.update,
        delete: request.delete,
        view: request.view,
        updated_at: new Date(),
      };
      const payload = qs.stringify(requestBody);
      try {
        const { data } = await patch(`/menus/${id}`, {
          data: payload,
        });
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormMenu = false;
      }
    },
    async deleteMenu(id) {
      this.loadFormMenu = true;
      try {
        await destroy(`/menus/${id}`);
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormMenu = false;
      }
    },
  },
};
