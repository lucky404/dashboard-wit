import { get, post, patch, destroy } from '@/services/Axios';
import qs from 'qs';
import errorHandler from '@/mixins/errorHandler';

export default {
  mixins: [errorHandler],
  data() {
    return {
      variable: {},
      variables: [],
      loadVariables: true,
      loadFormVariable: false,
      loadVariable: false,
    };
  },
  methods: {
    async getVariables() {
      this.loadVariables = true;
      try {
        const { data } = await get('/variables');
        this.variables = data;
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadVariables = false;
      }
    },
    async getVariable(id) {
      this.loadVariable = true;
      try {
        const { data } = await get(`/variables/${id}`);
        this.variable = data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadVariable = false;
      }
    },
    async createVariable(request) {
      this.loadFormVariable = true;
      const requestBody = {
        varId: request.varId,
        tabs: request.tabs,
        parameters: request.parameters,
        description: request.description,
        created_at: new Date(),
        updated_at: new Date(),
      };
      const payload = qs.stringify(requestBody);
      try {
        const { data } = await post('/variables', {
          data: payload,
        });
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormVariable = false;
      }
    },
    async updateVariable(request, id) {
      this.loadFormVariable = true;
      const requestBody = {
        varId: request.varId,
        tabs: request.tabs,
        parameters: request.parameters,
        description: request.description,
        updated_at: new Date(),
      };
      const payload = qs.stringify(requestBody);
      try {
        const { data } = await patch(`/variables/${id}`, {
          data: payload,
        });
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormVariable = false;
      }
    },
    async deleteVariable(id) {
      this.loadFormVariable = true;
      try {
        await destroy(`/variables/${id}`);
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormVariable = false;
      }
    },
  },
};
