import errorHandler from '@/mixins/errorHandler';
import qs from 'qs';

export default {
  mixins: [errorHandler],
  data() {
    return {
      loadingAuth: false,
    };
  },
  methods: {
    async login(request) {
      this.loadingAuth = true;
      const requestBody = {
        username: request.username,
        password: request.password,
      };
      const data = qs.stringify(requestBody);
      try {
        const user = await this.$store.dispatch('login', data);
        return user;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadingAuth = false;
      }
    },
  },
};
