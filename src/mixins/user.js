import { get, post, patch, destroy } from '@/services/Axios';
import qs from 'qs';
import errorHandler from '@/mixins/errorHandler';
import { mapGetters } from 'vuex';

export default {
  mixins: [errorHandler],
  data() {
    return {
      user: {},
      users: [],
      loadUsers: true,
      loadFormUser: false,
      loadUser: false,
    };
  },
  computed: mapGetters(['account']),
  methods: {
    async getUsers() {
      this.loadUsers = true;
      try {
        const { data } = await get('/users');
        const user = data.filter(
          (user) => user.username !== this.account.username
        );
        this.users = user;
        return user;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadUsers = false;
      }
    },
    async getUser(id) {
      this.loadUser = true;
      try {
        const { data } = await get(`/users/${id}`);
        this.user = data;
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadUser = false;
      }
    },
    async createUser(request) {
      this.loadFormUser = true;
      try {
        const pass = await this.generatePassword(request.password);
        const requestBody = {
          username: request.username,
          fullname: request.fullname,
          phone: request.phone,
          email: request.email,
          last_login: '',
          last_active: '',
          status: request.status ? 'blocked' : 'offline',
          isActive: false,
          nik: request.nik,
          password: pass,
          department: request.department,
          role: request.role,
          creator: {
            username: this.account.username,
            fullname: this.account.fullname,
            phone: this.account.phone,
            email: this.account.email,
          },
          created_at: new Date(),
          updated_at: new Date(),
        };
        const payload = qs.stringify(requestBody);
        const { data } = await post('/users', {
          data: payload,
        });
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormUser = false;
      }
    },
    async updateUser(request, id) {
      this.loadFormUser = true;
      const requestBody = {
        username: request.username,
        fullname: request.fullname,
        phone: request.phone,
        email: request.email,
        nik: request.nik,
        department: request.department,
        role: request.role,
        updated_at: new Date(),
      };
      const payload = qs.stringify(requestBody);
      try {
        if (request.password) {
          const pass = await this.generatePassword(request.password);
          requestBody.password = pass;
        }
        const { data } = await patch(`/users/${id}`, {
          data: payload,
        });
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormUser = false;
      }
    },
    async deleteUser(id) {
      this.loadFormUser = true;
      try {
        await destroy(`/users/${id}`);
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormUser = false;
      }
    },
    async generatePassword(password) {
      const requestBody = { password };
      const payload = qs.stringify(requestBody);
      try {
        const {
          data: { password },
        } = await post(`/generate-password/`, { data: payload });
        return password;
      } catch (e) {
        throw this.handleError(e);
      }
    },
    async logOutUser(id) {
      this.loadFormUser = true;
      try {
        const { data } = await post(`/logout/${id}`);
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormUser = false;
      }
    },
  },
};
