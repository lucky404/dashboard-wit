export default {
  data() {
    return {
      error: {
        message: '',
      },
    };
  },
  methods: {
    handleError(err) {
      if (err.response) {
        let message = '';
        if (err.response.data) {
          message = err.response.data.message;
        }
        if (err.response.status === 404) {
          this.$router.push({ name: '404', params: { error: message } });
        } else if (
          err.response.status === 401 &&
          err.response.data.error_code !== 'unauthorized'
        ) {
          this.$router.push({ name: '401', params: { error: message } });
        } else if (err.response.status === 500) {
          this.$router.push({ name: '500' });
        } else if (err.response.status === 400) {
          this.error.message = message;
          if (err.response.data) {
            const code = err.response.data.error_code;
            return {
              errorCode: code,
              message,
            };
          }
        }
      } else {
        const message = 'Terjadi suatu kekeliruan';
        this.error.message = message;
        return { message };
      }
    },
    clearError() {
      this.error.message = '';
    },
  },
};
