import { get, post, patch, destroy } from '@/services/Axios';
import qs from 'qs';
import errorHandler from '@/mixins/errorHandler';

export default {
  mixins: [errorHandler],
  data() {
    return {
      log: {},
      logs: [],
      loadLogs: true,
      loadFormLog: false,
      loadLog: false,
    };
  },
  methods: {
    async getLogs() {
      this.loadLogs = true;
      try {
        const { data } = await get('/logs');
        this.logs = data;
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadLogs = false;
      }
    },
    async getLog(id) {
      this.loadLog = true;
      try {
        const { data } = await get(`/logs/${id}`);
        this.log = data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadLog = false;
      }
    },
    async createLog(request) {
      this.loadFormLog = true;
      const requestBody = {
        username: request.username,
        ip: request.ip,
        action: request.action,
        description: request.description,
        created_at: new Date(),
        updated_at: new Date(),
      };
      const payload = qs.stringify(requestBody);
      try {
        const { data } = await post('/logs', {
          data: payload,
        });
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormLog = false;
      }
    },
    async updateLog(request, id) {
      this.loadFormLog = true;
      const requestBody = {
        username: request.username,
        ip: request.ip,
        action: request.action,
        description: request.description,
        updated_at: new Date(),
      };
      const payload = qs.stringify(requestBody);
      try {
        const { data } = await patch(`/logs/${id}`, {
          data: payload,
        });
        return data;
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormLog = false;
      }
    },
    async deleteLog(id) {
      this.loadFormLog = true;
      try {
        await destroy(`/logs/${id}`);
      } catch (e) {
        throw this.handleError(e);
      } finally {
        this.loadFormLog = false;
      }
    },
  },
};
