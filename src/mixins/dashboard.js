import { get } from '@/services/Axios';
import errorHandler from '@/mixins/errorHandler';
export default {
  mixins: [errorHandler],
  data() {
    return {
      barTrending: {
        series: [],
        chartOptions: {
          chart: {
            animations: {
              enabled: true,
              easing: 'linear',
              dynamicAnimation: {
                speed: 1000,
              },
            },
            toolbar: {
              show: false,
            },
            zoom: {
              enabled: true,
            },
          },
          dataLabels: {
            enabled: false,
          },
          legend: {
            tooltipHoverFormatter: function (val, opts) {
              return (
                val +
                ' - ' +
                opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] +
                ''
              );
            },
          },
          stroke: {
            curve: 'smooth',
          },
          colors: ['#4765b6', '#FF9800'],
          xaxis: {
            categories: [],
          },
        },
      },
      loading: {
        barTrending: false,
      },
    };
  },
  methods: {
    async fetchBarTrending() {
      this.loading.barTrending = true;
      try {
        const { data } = await get('/dashboard');
        this.barTrending.chartOptions.xaxis.categories = data.label;
        this.barTrending.series = [
          {
            name: 'Male',
            data: data.value.male,
          },
          {
            name: 'Female',
            data: data.value.female,
          },
        ];
        return data;
      } catch (e) {
        throw new Error(this.handleError(e));
      } finally {
        this.loading.barTrending = false;
      }
    },
  },
};
