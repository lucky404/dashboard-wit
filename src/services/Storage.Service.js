import { parse, stringify } from 'qs';
const TOKEN_KEY = 'access_token';
const USER = 'access_user';
const TOKEN_FCM = 'TOKEN_FCM';

/**
 * Manage the how Access Tokens are being stored and retreived from storage.
 *
 * Current implementation stores to localStorage. Local Storage should always be
 * accessed through this instace.
 **/
const TokenService = {
  getToken() {
    return localStorage.getItem(TOKEN_KEY);
  },
  getUser() {
    return parse(localStorage.getItem(USER));
  },
  updateUser(accessUser) {
    localStorage.setItem(USER, stringify(accessUser));
  },
  saveToken(accessToken, accessUser) {
    localStorage.setItem(TOKEN_KEY, accessToken);
    localStorage.setItem(USER, stringify(accessUser));
  },
  removeToken() {
    localStorage.removeItem(TOKEN_KEY);
    localStorage.removeItem(USER);
    localStorage.removeItem(TOKEN_FCM);
  },
};

export { TokenService };
