import axios from 'axios';
import { mapKeys, mapValues, camelCase, snakeCase } from 'lodash';
import { TokenService } from '@/services/Storage.Service.js';
import qs from 'qs';

const { VUE_APP_API_URL } = process.env;

function getAccessToken() {
  // @todo: load access token from cookie or locale storage
  return TokenService.getToken() || '';
}

function transformKeys(data, iteratee) {
  if (Array.isArray(data)) {
    return data.map((d) => transformKeys(d, iteratee));
  }

  if (data instanceof Object) {
    return mapValues(
      mapKeys(data, (_, k) => iteratee(k)),
      (v) => transformKeys(v, iteratee)
    );
  }

  return data;
}

export function camelCaseKeys(data) {
  return transformKeys(data, camelCase);
}

export function snakeCaseKeys(data) {
  return transformKeys(data, snakeCase);
}

// @see: https://github.com/mzabriskie/axios#axios-api
export function request(method, url, config = {}, options = {}) {
  const { params, data, headers, maxContentLength } = config;

  // non-axios specific params
  const { suppressAuth } = options;

  const baseURL = VUE_APP_API_URL;

  // @see: https://tools.ietf.org/html/rfc6750
  // const bearerToken = `Bearer ${getAccessToken()}`
  const bearerToken = getAccessToken();

  return new Promise((resolve, reject) => {
    axios.defaults.paramsSerializer = function (params) {
      return qs.stringify(params);
    };
    axios({
      method,
      baseURL,
      url,
      params,
      data: data,
      // headers: suppressAuth ? headers : { ...headers, Authorization: bearerToken, "x-auth": xauth },
      headers: suppressAuth
        ? headers
        : { ...headers, authorization: bearerToken },
      maxContentLength,
    })
      .then((response) => {
        resolve({
          ...response,
          data: camelCaseKeys(response.data.data),
        });
      })
      .catch(reject);
  });
}

export function get(url, config, options) {
  return request('GET', url, config, options);
}

export function post(url, config, options) {
  return request('POST', url, config, options);
}

export function put(url, config, options) {
  return request('PUT', url, config, options);
}

export function patch(url, config, options) {
  return request('PATCH', url, config, options);
}

// not "delete()" because of reserved word
export function destroy(url, config, options) {
  return request('DELETE', url, config, options);
}
