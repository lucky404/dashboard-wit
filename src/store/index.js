import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import { post, get } from '@/services/Axios';

import { TokenService } from '@/services/Storage.Service.js';

Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    status: '',
    token: TokenService.getToken() || '',
    user: TokenService.getUser() || {},
  },
  mutations: {
    auth_request(state) {
      state.status = 'loading';
    },
    auth_success(state, data) {
      state.status = 'success';
      state.token = data.token;
      state.user = data;
    },
    auth_error(state) {
      state.status = 'error';
    },
    logout(state) {
      state.status = '';
      state.token = '';
      state.user = {};
    },
    update_profile(state, data) {
      state.user = data;
    },
  },
  getters: {
    isLoggedIn: (state) => !!state.token,
    account: (state) => state.user,
    authStatus: (state) => state.status,
  },
  actions: {
    async login({ commit }, request) {
      commit('auth_request');
      try {
        const { data } = await post('/signin', { data: request });
        const token = data.token;
        const user = data;
        TokenService.saveToken(token, user);
        axios.defaults.headers.common.authorization = token;
        commit('auth_success', data);
        return user;
      } catch (e) {
        commit('auth_error');
        TokenService.removeToken();
        throw e;
      }
    },
    async getProfile({ commit, state }) {
      const { data } = await get(`/users/${state.user.id}`);
      commit('update_profile', data);
      TokenService.updateUser(data);
      return data;
    },
    async logout({ commit, state, dispatch }) {
      try {
        await post(`/logout/${state.user.id}`);
        dispatch('clearSessions');
      } catch (e) {
        commit('auth_error');
        throw e;
      }
    },
    async clearSessions({ commit }) {
      TokenService.removeToken();
      delete axios.defaults.headers.common.authorization;
      commit('logout');
    },
  },
});
