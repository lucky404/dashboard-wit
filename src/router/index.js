import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store/index.js';
import routes from '@/router/routes';

Vue.use(VueRouter);

const router = new VueRouter({
  linkExactActiveClass: 'active',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '*',
      redirect: { name: 'dashboard' },
    },
  ].concat(routes),
});

router.beforeEach((to, from, next) => {
  const authenticated = store.getters.isLoggedIn;
  const onlyLoggedOut = to.matched.some((record) => record.meta.onlyLoggedOut);
  const isPublic = to.matched.some((record) => record.meta.public);
  if (!isPublic && !authenticated) {
    return next({
      path: '/login',
    });
  }
  if (authenticated && onlyLoggedOut) {
    return next('/dashboard');
  }
  document.title = `Dashboard Admin | ${to.meta.title}`;
  next();
});

export default router;
