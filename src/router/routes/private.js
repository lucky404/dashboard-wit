const Dashboard = () =>
  import(
    /* webpackChunkName: "dashboard.Dashboard" */ '@/views/private/Dashboard.vue'
  );
const UserList = () =>
  import(
    /* webpackChunkName: "dashboard.user" */ '@/views/private/user/List.vue'
  );
const UserForm = () =>
  import(
    /* webpackChunkName: "dashboard.user" */ '@/views/private/user/Form.vue'
  );
const LogList = () =>
  import(
    /* webpackChunkName: "dashboard.Log" */ '@/views/private/log/List.vue'
  );
const VariableList = () =>
  import(
    /* webpackChunkName: "dashboard.variable" */ '@/views/private/variable/List.vue'
  );
const SessionList = () =>
  import(
    /* webpackChunkName: "dashboard.Session" */ '@/views/private/session/List.vue'
  );
const RoleList = () =>
  import(
    /* webpackChunkName: "dashboard.Role" */ '@/views/private/role/List.vue'
  );
const AccessList = () =>
  import(
    /* webpackChunkName: "dashboard.Role" */ '@/views/private/role/Access.vue'
  );
const MenuList = () =>
  import(
    /* webpackChunkName: "dashboard.Menu" */ '@/views/private/menu/List.vue'
  );
const MenuForm = () =>
  import(
    /* webpackChunkName: "dashboard.Menu" */ '@/views/private/menu/Form.vue'
  );
const routes = [
  {
    path: '/dashboard',
    name: 'dashboard',
    component: Dashboard,
    meta: {
      title: 'Dashboard',
    },
  },
  {
    path: '/user',
    name: 'user',
    component: UserList,
    meta: {
      title: 'User Management',
    },
  },
  {
    path: '/user/form',
    name: 'addUser',
    component: UserForm,
    meta: {
      title: 'Add User',
    },
  },
  {
    path: '/user/form/:id',
    name: 'editUser',
    component: UserForm,
    meta: {
      title: 'Edit User',
    },
  },
  {
    path: '/log',
    name: 'Log',
    component: LogList,
    meta: {
      title: 'Log Activity',
    },
  },
  {
    path: '/variable',
    name: 'variable',
    component: VariableList,
    meta: {
      title: 'Variable Parameters',
    },
  },
  {
    path: '/session',
    name: 'session',
    component: SessionList,
    meta: {
      title: 'Session Management',
    },
  },
  {
    path: '/role',
    name: 'role',
    component: RoleList,
    meta: {
      title: 'Role Management',
    },
  },
  {
    path: '/role/access',
    name: 'access',
    component: AccessList,
    meta: {
      title: 'Access Settings',
    },
  },
  {
    path: '/menu',
    name: 'menu',
    component: MenuList,
    meta: {
      title: 'Menu Management',
    },
  },
  {
    path: '/menu/form',
    name: 'addMenu',
    component: MenuForm,
    meta: {
      title: 'Add Menu',
    },
  },
  {
    path: '/menu/form/:id',
    name: 'editMenu',
    component: MenuForm,
    meta: {
      title: 'Edit Menu',
    },
  },
];

export default routes.map((route) => {
  const meta = {
    public: false,
    title: route.meta.title,
  };
  return { ...route, meta };
});
