const Login = () =>
  import(/* webpackChunkName: "dashboard.Login" */ '@/views/public/Login.vue');

const error404 = () =>
  import(
    /* webpackChunkName: "dashboard.errorPage" */ '@/views/public/404.vue'
  );
const error500 = () =>
  import(
    /* webpackChunkName: "dashboard.errorPage" */ '@/views/public/500.vue'
  );
const error401 = () =>
  import(
    /* webpackChunkName: "dashboard.errorPage" */ '@/views/public/401.vue'
  );

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
    onlyLoggedOut: true,
    meta: {
      title: 'Masuk',
    },
  },
  {
    path: '/404',
    name: '404',
    component: error404,
    meta: {
      title: '404',
    },
  },
  {
    path: '/500',
    name: '500',
    component: error500,
    meta: {
      title: '500',
    },
  },
  {
    path: '/401',
    name: '401',
    component: error401,
    meta: {
      title: '401',
    },
  },
];
export default routes.map((route) => {
  const meta = {
    public: true,
    title: route.meta.title,
    onlyLoggedOut: route.onlyLoggedOut,
  };
  return { ...route, meta };
});
